/* Copyright 2021,
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "Communicator.h"
#include <stdint.h>
#include "ADS1299.h"
#include "systick.h"
#include "lpc_types.h"
#include "stopwatch.h"

/*==================[macros and definitions]=================================*/
#define COMM_MULTIBYTE_CHANNEL_SETTINGS_MAX_COUNTER 7
#define COMM_MULTIBYTE_LEAD_OFF_MAX_COUNTER 3
#define COMM_MULTIBYTE_SAMPLE_RATE_MAX_COUNTER 1
#define COMM_MULTIBYTE_MODE_MAX_COUNTER 1
#define COMM_TIMEOUT_MULTIBYTE_COMMAND 1000

/*==================[internal data definition]===============================*/
typedef enum {
	COMM_MULTIBYTE_CMD_CHANN_SETTINGS,
	COMM_MULTIBYTE_CMD_LEADOFF_SETTINGS,
	COMM_MULTIBYTE_CMD_SAMPLE_RATE,
	COMM_MULTIBYTE_CMD_BOARD_MODE
}communicator_multibyte_command_t;

/*==================[internal data declaration]==============================*/
/**
 * @brief Flag to indicate if commands are being processed correctly.
 */
bool error_check;

/**
 * @brief Flag to indicate if multibyte settings commands are being received.
 */
bool get_multibyte_command = FALSE;

/**
 * @brief Counter used to retrieve commands from serial port.
 */
uint8_t multibyte_counter = 0;

/**
 * @brief Multibyte command is being retrieve from serial port.
 */
communicator_multibyte_command_t multibyte_command = 0;

/**
 * @brief Keep track of what channel we are loading settings for.
 */
channel_ADS1299_t current_channel_to_set = ADS1299_CHANNEL1;

/**
 * @brief Lead off channel detecting state.
 */
bool leadoff_p_state = 0, leadoff_n_state = 0;

/**
 * @brief Store new settings for a channel.
 */
CHnSet_ADS1299_t channel_settings_to_set;

/**
 * @brief Store board mode.
 */
//communicator_board_mode_t board_mode = COMM_BOARD_MODE_DEFAULT;
communicator_board_mode_t board_mode = COMM_BOARD_MODE_DEBUG;

/**
 * @brief Store board mode string.
 */
uint8_t board_mode_string[10];

/*==================[internal functions declaration]=========================*/
/**
 * @brief Function for receiving new commands from UART interrupt.
 */
void CommunicatorSerialInt(void);

/**
 * @brief Function for interpreting multibyte commands and configure channel settings.
 * @param token Command to be decoded and interpreted.
 */
void CommunicatorProcessChannelSettings(uint8_t token);

/**
 * @brief Function for interpreting multibyte commands and configure lead off settings.
 * @param token Command to be decoded and interpreted.
 */
void CommunicatorProcessLeadoffSettings(uint8_t token);


/**
 * @brief Function for interpreting multibyte commands and configure sample rate.
 * @param token Command to be decoded and interpreted.
 */
void CommunicatorProcessSampleRate(uint8_t token);


/**
 * @brief Function for interpreting multibyte commands and configure board mode.
 * @param token Command to be decoded and interpreted
 */
void CommunicatorProcessBoardMode(uint8_t token);

/**
* @brief This is a function that can be called multiple times, this is
* what we refer to as a `soft reset`. You will hear/see this many times.
*/
void CommunicatorStartUpMsj(void);

/**
 * @brief Print the current Default Channel Settings.
 */
void CommunicatorPrintDefaultChannelSettings();

/**
 * @brief Setup the timeout counter for multibyte commands. Use systick counter.
 * Interrupt in 1 second after this function is called.
 */
void CommunicatorStartTimeoutMultibyteCommand(void);

/**
 * @brief Stop the timeout counter for multibyte commands.
 */
void CommunicatorEndTimeoutMultibyteCommand(void);

/**
 * @brief Print the current board mode.
 * @param get Board mode.
 * @return String pointer with board mode description.
 */
const char* CommunicatorGetBoardMode(communicator_board_mode_t get);

/**
* @brief Print the current state of all ADS1299 registers
*/
void CommunicatorPrintRegisters(void);

/**
* @brief To get the name of a specific ADS1299 register
* @param regNumber Number of register in decimal (0-23)
* @return String pointer with name of register
*/
const char* CommunicatorADS1299RegisterName(uint8_t reg_number);

/**
* @brief Print all streaming commands
*/
void CommunicatorPrintMenu(void);

/**
 * @brief Convert channel number from character to number
 * @param n Character defining channel: "1-8", "Q-I"
 * @return Channel number
 */
uint8_t CommunicatorGetChannelNumber(uint8_t n);

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

void CommunicatorInit(void) {
	UARTInit(COMM_UART, COMM_BAUD_RATE);
	UARTInit(COMM_DEBUG_UART, COMM_DEBUG_BAUD_RATE);
	UARTActivInt(COMM_UART, &CommunicatorSerialInt);
	ADS1299ActivateUARTStreaming();
	CommunicatorStartUpMsj();
}

void CommunicatorSerialInt(void) {
	uint8_t in_char;
	UARTReadByte(COMM_UART, &in_char);
	error_check = CommunicatorProccessCommand(in_char);
}

bool CommunicatorProccessCommand(uint8_t token) {
	if(get_multibyte_command) { /* NOTE: if not all commands are not received within 1 second, Timeout processing multi byte message - please send all commands at once as of v2$$$ */
		multibyte_counter++;
		switch (multibyte_command) {
		case COMM_MULTIBYTE_CMD_CHANN_SETTINGS:
			CommunicatorProcessChannelSettings(token);
			break;
		case COMM_MULTIBYTE_CMD_LEADOFF_SETTINGS:
			CommunicatorProcessLeadoffSettings(token);
			break;
		case COMM_MULTIBYTE_CMD_SAMPLE_RATE:
			CommunicatorProcessSampleRate(token);
			break;
		case COMM_MULTIBYTE_CMD_BOARD_MODE:
			CommunicatorProcessBoardMode(token);
			break;
		default:
			return FALSE;
			break;
		}
	}
	else {
		if(board_mode == COMM_BOARD_MODE_DEBUG) {
			UARTSendString(COMM_DEBUG_UART, (uint8_t*)"pC: ");
			UARTSendByte(COMM_DEBUG_UART, &token);
			UARTSendString(COMM_DEBUG_UART, (uint8_t*)"\r\n");
		}

		switch (token) {
		//Turn channels on/off commands
		case '1':
			ADS1299DeactivateChannel(ADS1299_CHANNEL1);
			break;
		case '2':
			ADS1299DeactivateChannel(ADS1299_CHANNEL2);
			break;
		case '3':
			ADS1299DeactivateChannel(ADS1299_CHANNEL3);
			break;
		case '4':
			ADS1299DeactivateChannel(ADS1299_CHANNEL4);
			break;
		case '5':
			ADS1299DeactivateChannel(ADS1299_CHANNEL5);
			break;
		case '6':
			ADS1299DeactivateChannel(ADS1299_CHANNEL6);
			break;
		case '7':
			ADS1299DeactivateChannel(ADS1299_CHANNEL7);
			break;
		case '8':
			ADS1299DeactivateChannel(ADS1299_CHANNEL8);
			break;
		case '!':
			ADS1299ActivateChannel(ADS1299_CHANNEL1);
			break;
		case '@':
			ADS1299ActivateChannel(ADS1299_CHANNEL2);
			break;
		case '#':
			ADS1299ActivateChannel(ADS1299_CHANNEL3);
			break;
		case '$':
			ADS1299ActivateChannel(ADS1299_CHANNEL4);
			break;
		case '%':
			ADS1299ActivateChannel(ADS1299_CHANNEL5);
			break;
		case '^':
			ADS1299ActivateChannel(ADS1299_CHANNEL6);
			break;
		case '&':
			ADS1299ActivateChannel(ADS1299_CHANNEL7);
			break;
		case '*':
			ADS1299ActivateChannel(ADS1299_CHANNEL8);
			break;

			// Test signal control commands
		case '0':
			ADS1299ChangeAllChannelInputTypeEqual(ADS1299_SHORTED);
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*)("Success: Configured internal test signal.$$$\r\n"));
			}
			break;
		case '-':
			ADS1299ChangeAllChannelInputTypeEqual(ADS1299_TESTSIG);
			ADS1299ConfigTestSignal(ADS1299_ADSTESTSIG_PULSE_SLOW, ADS1299_ADSTESTSIG_AMP_1X);
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*)("Success: Configured internal test signal.$$$\r\n"));
			}
			break;
		case '=':
			ADS1299ChangeAllChannelInputTypeEqual(ADS1299_TESTSIG);
			ADS1299ConfigTestSignal(ADS1299_ADSTESTSIG_PULSE_FAST, ADS1299_ADSTESTSIG_AMP_1X);
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*)("Success: Configured internal test signal.$$$\r\n"));
			}
			break;
		case 'p':
			ADS1299ChangeAllChannelInputTypeEqual(ADS1299_TESTSIG);
			ADS1299ConfigTestSignal(ADS1299_ADSTESTSIG_DCSIG, ADS1299_ADSTESTSIG_AMP_2X);
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*)("Success: Configured internal test signal.$$$\r\n"));
			}
			break;
		case '[':
			ADS1299ChangeAllChannelInputTypeEqual(ADS1299_TESTSIG);
			ADS1299ConfigTestSignal(ADS1299_ADSTESTSIG_PULSE_SLOW, ADS1299_ADSTESTSIG_AMP_2X);
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*)("Success: Configured internal test signal.$$$\r\n"));
			}
			break;
		case ']':
			ADS1299ChangeAllChannelInputTypeEqual(ADS1299_TESTSIG);
			ADS1299ConfigTestSignal(ADS1299_ADSTESTSIG_PULSE_FAST, ADS1299_ADSTESTSIG_AMP_2X);
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*)("Success: Configured internal test signal.$$$\r\n"));
			}
			break;

			// Channel settings commands
		case 'x':
			get_multibyte_command = TRUE;
			multibyte_counter = 0;
			multibyte_command = COMM_MULTIBYTE_CMD_CHANN_SETTINGS;
			CommunicatorStartTimeoutMultibyteCommand();
			break;

		case 'X':
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*) ("Failure: too few chars$$$\r\n"));
			}
			break;
		case 'd':
			//ADS1299SetChannelsToDefaultConfig();
			ADS1299SetChannelsToDefaultConfigForEEG();
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*) ("Updating channel settings to default$$$\r\n"));
			}
			break;
		case 'D':
			CommunicatorPrintDefaultChannelSettings();
			break;

			// Lead-off impedance detection commands
		case 'z':
			get_multibyte_command = TRUE;
			multibyte_counter = 0;
			multibyte_command = COMM_MULTIBYTE_CMD_LEADOFF_SETTINGS;
			CommunicatorStartTimeoutMultibyteCommand();
			break;
		case 'Z':
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*) ("Failure: too few chars$$$\r\n"));
			}
			break;

			// Change sampling frequency
		case 'A':
			//SD for 5min
			break;
		case 'S':
			//SD for 15min
			break;
		case 'F':
			//SD for 30min
			break;
		case 'G':
			//SD for 1hs
			break;
		case 'H':
			//SD for 2hs
			break;
		case 'J':
			//SD for 4hs
			break;
		case 'K':
			//SD for 12hs
			break;
		case 'L':
			//SD for 24hs
			break;
		case 'a':
			//SD for 14seg (test)
			break;
		case 'j':
			//SD stop
			break;

			//Stream data and filter commands
		case 'b':
			ADS1299StartStreaming();
			break;
		case 's':
			ADS1299StopStreaming();
			break;

			// Initialize and verify
		case '?':
			CommunicatorPrintRegisters(); // Query the ADS registers
			break;
		case 'v':
//			ADS1299Init(); //Quitado utilizando el mock por que buguea el RIT timer - hacer una funcion de soft reset
			CommunicatorStartUpMsj();
			break;

			// Slave board commands
		case 'q':
	//		ADS1299DeactivateChannel(ADS1299_CHANNEL9);
			break;
		case 'w':
	//		ADS1299DeactivateChannel(ADS1299_CHANNEL10);
			break;
		case 'e':
	//		ADS1299DeactivateChannel(ADS1299_CHANNEL11);
			break;
		case 'r':
	//		ADS1299DeactivateChannel(ADS1299_CHANNEL12);
			break;
		case 't':
	//		ADS1299DeactivateChannel(ADS1299_CHANNEL13);
			break;
		case 'y':
	//		ADS1299DeactivateChannel(ADS1299_CHANNEL14);
			break;
		case 'u':
	//		ADS1299DeactivateChannel(ADS1299_CHANNEL15);
			break;
		case 'i':
	//		ADS1299DeactivateChannel(ADS1299_CHANNEL16);
			break;
		case 'Q':
	//		ADS1299ActivateChannel(ADS1299_CHANNEL9);
			break;
		case 'W':
	//		ADS1299ActivateChannel(ADS1299_CHANNEL10);
			break;
		case 'E':
	//		ADS1299ActivateChannel(ADS1299_CHANNEL11);
			break;
		case 'R':
	//		ADS1299ActivateChannel(ADS1299_CHANNEL12);
			break;
		case 'T':
	//		ADS1299ActivateChannel(ADS1299_CHANNEL13);
			break;
		case 'Y':
	//		ADS1299ActivateChannel(ADS1299_CHANNEL14);
			break;
		case 'U':
	//		ADS1299ActivateChannel(ADS1299_CHANNEL15);
			break;
		case 'I':
	//		ADS1299ActivateChannel(ADS1299_CHANNEL16);
			break;

		case 'c':	// use 8 channel mode
//			if(Drv_ADS1299_getSlavePresent())
//			{
//				Drv_ADS1299_removeSlave();
			UARTSendString(COMM_UART, (uint8_t*)"daisy removed$$$\r\n");
//			}
//			outputType = OUTPUT_8_CHAN;
			break;

		case 'C':	// use 16 channel mode
//			echo = 'C';
//			UARTSendByte(COMM_UART, &echo);

//			if(!(Drv_ADS1299_getSlavePresent()))
//			{
//				Drv_ADS1299_attachSlave();
//			UARTSendString(COMM_UART, (uint8_t*)"daisy attached16$$$");
//			}
//
//			if(Drv_ADS1299_getSlavePresent())
//			{
//			UARTSendString(COMM_UART, (uint8_t*)"16$$$\r\n");
//				outputType = OUTPUT_16_CHAN;
//			}
//			else
//			{
//				outputType = OUTPUT_8_CHAN;
			UARTSendString(COMM_UART, (uint8_t*)"no daisy to attach!8$$$");
//			}
			break;

			// Sampling frequency command
		case '~':
			get_multibyte_command = TRUE;
			multibyte_command = COMM_MULTIBYTE_CMD_SAMPLE_RATE;
			CommunicatorStartTimeoutMultibyteCommand();
			break;

			// board mode command
		case '/':
			get_multibyte_command = TRUE;
			multibyte_command = COMM_MULTIBYTE_CMD_BOARD_MODE;
			CommunicatorStartTimeoutMultibyteCommand();
			break;

		case 'M':
			CommunicatorPrintMenu();
			break;

		default:
			if(board_mode == COMM_BOARD_MODE_DEBUG) {
				UARTSendString(COMM_DEBUG_UART, (uint8_t*)"Error token.$$$\r\n");
			}
			return FALSE;
			break;
		}
	}
	return TRUE;
}

void CommunicatorProcessChannelSettings(uint8_t token) {
	if(token == 'X' && multibyte_counter<=COMM_MULTIBYTE_CHANNEL_SETTINGS_MAX_COUNTER) {
		//Error: too few arguments - abort
		get_multibyte_command = FALSE;
		multibyte_counter = 0;
		CommunicatorEndTimeoutMultibyteCommand();
		if(!ADS1299IsRunning()) {
			UARTSendString(COMM_UART, (uint8_t*)"Failure: too few chars$$$\r\n");
		}
	}
	else {
		switch (multibyte_counter) {
		case 1: // channel number
			current_channel_to_set = CommunicatorGetChannelNumber(token);
			break;
		case 2: // POWER_DOWN
			channel_settings_to_set.power_down = token - '0';
			break;
		case 3: // GAIN_SET
			channel_settings_to_set.pga_gain = (token - '0')<<4;
			break;
		case 4: // INPUT_TYPE_SET
			channel_settings_to_set.channel_input = token - '0';
			break;
		case 5: // BIAS_SET
			channel_settings_to_set.set_bias = token - '0';
			break;
		case 6: // SRB2_SET
			channel_settings_to_set.use_srb2 = token - '0';
			break;
		case 7: // SRB1_SET
			channel_settings_to_set.use_srb1 = token - '0';
			break;
		case 8: // 'X' latch
			if (token != 'X') {
				if (!ADS1299IsRunning()) {
					UARTSendString(COMM_UART, (uint8_t*)"Failure: 9th char not X$$$\r\n");
				}
				get_multibyte_command = FALSE;
				multibyte_counter = 0;
				CommunicatorEndTimeoutMultibyteCommand();
			}
			else {
				get_multibyte_command = FALSE;
				multibyte_counter = 0;
				CommunicatorEndTimeoutMultibyteCommand();
				ADS1299ConfigOneChannel(current_channel_to_set, channel_settings_to_set);
				if (!ADS1299IsRunning()) {
					UARTSendString(COMM_UART, (uint8_t*) ("Success: Channel set for "));
					UARTSendString(COMM_UART, (uint8_t*) UARTItoa(current_channel_to_set+1, 10));
					UARTSendString(COMM_UART, (uint8_t*) ("$$$\r\n"));
				}
			}
			break;
		default: // should have exited
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*)"Failure: Err: too many chars$$$\r\n");
			}
			get_multibyte_command = FALSE;
			multibyte_counter = 0;
			CommunicatorEndTimeoutMultibyteCommand();
			break;
		}
	}
}

void CommunicatorProcessLeadoffSettings(uint8_t token) {
	if(token == 'Z' && multibyte_counter<=COMM_MULTIBYTE_LEAD_OFF_MAX_COUNTER) {
		//Error: too few arguments - abort
		get_multibyte_command = FALSE;
		multibyte_counter = 0;
		CommunicatorEndTimeoutMultibyteCommand();
		if(!ADS1299IsRunning()) {
			UARTSendString(COMM_UART, (uint8_t*)"Failure: too few chars$$$\r\n");
		}
	}
	else {
		switch (multibyte_counter) {
		case 1: // channel number
			current_channel_to_set = CommunicatorGetChannelNumber(token);
			break;
		case 2: // pchannel setting
			leadoff_p_state = token - '0';
			break;
		case 3: // nchannel setting
			leadoff_n_state = token - '0';
			break;
		case 4: // 'Z' latch
			if (token != 'Z') {
				if (!ADS1299IsRunning()) {
					UARTSendString(COMM_UART, (uint8_t*)"Failure: 5th char not Z$$$\r\n");
				}
				get_multibyte_command = FALSE;
				multibyte_counter = 0;
				CommunicatorEndTimeoutMultibyteCommand();
			}
			else {
				get_multibyte_command = FALSE;
				multibyte_counter = 0;
				CommunicatorEndTimeoutMultibyteCommand();
				ADS1299ChannelSelectLeadOffDetection(current_channel_to_set, leadoff_p_state, leadoff_n_state); //TODO: Probar cambios en las funciones de lead off
				if (!ADS1299IsRunning()) {
					UARTSendString(COMM_UART, (uint8_t*) ("Success: Lead off set for "));
					UARTSendString(COMM_UART, (uint8_t*) UARTItoa(current_channel_to_set+1, 10));
					UARTSendString(COMM_UART, (uint8_t*) ("$$$\r\n"));
				}
			}
			break;
		default: // should have exited
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*)"Failure: Err: too many chars$$$\r\n");
			}
			get_multibyte_command = FALSE;
			multibyte_counter = 0;
			CommunicatorEndTimeoutMultibyteCommand();
			break;
		}

	}
}

void CommunicatorProcessSampleRate(uint8_t token) {
	const uint16_t sampling_convert[7] = {16000, 8000, 4000, 2000, 1000, 500, 250};
	if(token == '~') {
		UARTSendString(COMM_UART, (uint8_t*) ("Sample rate is "));
		UARTSendString(COMM_UART, (uint8_t*) UARTItoa(sampling_convert[ADS1299GetSamplingFrecuency()], 10));
		UARTSendString(COMM_UART, (uint8_t*) ("Hz$$$\r\n"));
	}
	else if ((token-'0') >= 0 && (token-'9') <= 0) {
		if((token-'0') <= ADS1299_SAMPLE_RATE_250HZ){
			ADS1299ChangeSamplingFrecuency(token-'0');
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*) ("Success: Sample rate is "));
				UARTSendString(COMM_UART, (uint8_t*) UARTItoa(sampling_convert[ADS1299GetSamplingFrecuency()], 10));
				UARTSendString(COMM_UART, (uint8_t*) ("Hz$$$\r\n"));
			}

		}
		else {
			if (!ADS1299IsRunning()) {
				UARTSendString(COMM_UART, (uint8_t*)"Failure: sample value out of bounds$$$\r\n");
			}
		}
	}
	else {
		if (!ADS1299IsRunning()) {
			UARTSendString(COMM_UART, (uint8_t*)"Failure: invalid sample value$$$\r\n");
		}
	}
	get_multibyte_command = FALSE;
	CommunicatorEndTimeoutMultibyteCommand();
}

void CommunicatorProcessBoardMode(uint8_t token) {
	if(token == '/') {
		UARTSendString(COMM_UART, (uint8_t*) ("Board mode is "));
		UARTSendString(COMM_UART, (uint8_t*) CommunicatorGetBoardMode(board_mode));
		UARTSendString(COMM_UART, (uint8_t*) (".$$$\r\n"));
	}
	else if ((token-'0') >= 0 && (token-'9') <= 0) {
		if((token-'0') < COMM_BOARD_MODE_END){
			board_mode = (token - '0');
//			InterfaceChangeBoardMode(board_mode);
			UARTSendString(COMM_UART, (uint8_t*) ("Success: Board mode set to "));
			UARTSendString(COMM_UART, (uint8_t*) CommunicatorGetBoardMode(board_mode));
			UARTSendString(COMM_UART, (uint8_t*) (".$$$\r\n"));

		}
		else {
			UARTSendString(COMM_UART, (uint8_t*)"Failure: board mode value out of bounds.$$$\r\n");
		}
	}
	else {
		if (!ADS1299IsRunning()) {
			UARTSendString(COMM_UART, (uint8_t*)"Failure: invalid board mode value.$$$\r\n");
		}
	}
	get_multibyte_command = FALSE;
	CommunicatorEndTimeoutMultibyteCommand();
}

const char* CommunicatorGetBoardMode(communicator_board_mode_t get) {
	switch(get){
	case COMM_BOARD_MODE_DEBUG:
	    return "debug";
		break;
	case COMM_BOARD_MODE_ANALOG:
	    return "analog";
		break;
	case COMM_BOARD_MODE_DIGITAL:
	    return "digital";
		break;
	case COMM_BOARD_MODE_MARKER:
	    return "marker";
		break;
	case COMM_BOARD_MODE_DEFAULT:
	default:
	  return "default";
		break;
	}
}

void CommunicatorStartUpMsj(void) {

	UARTSendString(COMM_UART, (uint8_t*)("OpenBCI V3 8-16 channel\r\n"));
	UARTSendString(COMM_UART, (uint8_t*)("ADS1299 Device ID: 0x"));
	UARTSendString(COMM_UART, (uint8_t*) UARTItoa(ADS1299GetID(), 16));
	UARTSendString(COMM_UART, (uint8_t*)("\r\n"));
	UARTSendString(COMM_UART, (uint8_t*)("Firmware: v2.0.0\r\n"));
	UARTSendString(COMM_UART, (uint8_t*)("$$$\r\n"));
}

void CommunicatorPrintDefaultChannelSettings() {
	CHnSet_ADS1299_t* default_settings;
	default_settings = (CHnSet_ADS1299_t*) ADS1299GetDefaultConfig(); //get the array with default channel settings
	uint8_t dat; //buffer
	bool is_running_when_called = ADS1299IsRunning();

	ADS1299StopStreaming(); //stop running

	dat = default_settings->power_down + '0';
	UARTSendByte(COMM_UART, &(dat));
	dat = (default_settings->pga_gain>>4) + '0';
	UARTSendByte(COMM_UART, &(dat));
	dat = default_settings->channel_input + '0';
	UARTSendByte(COMM_UART, &(dat));
	dat = default_settings->set_bias + '0';
	UARTSendByte(COMM_UART, &(dat));
	dat = default_settings->use_srb2 + '0';
	UARTSendByte(COMM_UART, &(dat));
	dat = default_settings->use_srb1 + '0';
	UARTSendByte(COMM_UART, &(dat));
	UARTSendString(COMM_UART, (uint8_t*)"$$$\r\n");

	if (is_running_when_called == TRUE) {
		ADS1299StartStreaming();
	}

}

void CommunicatorStartTimeoutMultibyteCommand(void) {
	SystickInit(COMM_TIMEOUT_MULTIBYTE_COMMAND, CommunicatorEndTimeoutMultibyteCommand);
}

void CommunicatorEndTimeoutMultibyteCommand(void) {
	SystickDeinit();
	if(get_multibyte_command) {
		get_multibyte_command = FALSE;
		multibyte_counter = 0;
		if(!ADS1299IsRunning()) {
			UARTSendString(COMM_UART, (uint8_t*)"Timeout processing multi byte message - please send all commands at once as of v2$$$\r\n");
		}
	}
}

void CommunicatorPrintRegisters(void)
{
	uint8_t reg_data[24];
	bool slave_present = FALSE;
	uint8_t i = 0; //loop counter

//	slave_present = Drv_ADS1299_getSlavePresent(); //is the slave board present?
	ADS1299GetRegistersData(reg_data); //get the the current registers

	UARTSendString(COMM_UART, (uint8_t*)"\r\nBoard ADS Registers\r\n");

	for(i=0; i<24; i++) //send the 24 ADS1299 registers
	{

		UARTSendString(COMM_UART, (uint8_t*)CommunicatorADS1299RegisterName(i));
		UARTSendString(COMM_UART, (uint8_t*)"0x");
		UARTSendString(COMM_UART, (uint8_t*)UARTItoa(reg_data[i], 16)); // convert to hexadecimal
		UARTSendString(COMM_UART, (uint8_t*)"\r\n");
		StopWatch_DelayMs(10);
	}

	if(slave_present == TRUE)
	{
//		ADS1299GetSlaveRegistersData(regData); //get the the current registers

		UARTSendString(COMM_UART, (uint8_t*)"\r\nSlave ADS Registers\r\n");

		for(i=0; i<24; i++) //send the 24 ADS1299 registers
		{

			UARTSendString(COMM_UART, (uint8_t*)CommunicatorADS1299RegisterName(i));
			UARTSendString(COMM_UART, (uint8_t*)"0x");
			UARTSendString(COMM_UART, (uint8_t*)UARTItoa(reg_data[i], 16)); // convert to hexadecimal
			UARTSendString(COMM_UART, (uint8_t*)"\r\n");
		}
	}

	UARTSendString(COMM_UART, (uint8_t*)"$$$\r\n");
}

const char* CommunicatorADS1299RegisterName(uint8_t reg_number)
{
 switch(reg_number)
 {
	case 0:
		return "ADS_ID: ";
		break;
	case 1:
		return "CONFIG1: ";
		break;
	case 2:
		return "CONFIG2: ";
		break;
	case 3:
		return "CONFIG3: ";
		break;
	case 4:
		return "LOFF: ";
		break;
	case 5:
		return "CH1SET: ";
		break;
	case 6:
		return "CH2SET: ";
		break;
	case 7:
		return "CH3SET: ";
		break;
	case 8:
		return "CH4SET: ";
		break;
	case 9:
		return "CH5SET: ";
		break;
	case 10:
		return "CH6SET: ";
		break;
	case 11:
		return "CH7SET: ";
		break;
	case 12:
		return "CH8SET: ";
		break;
	case 13:
		return "BIAS_SENSP: ";
		break;
	case 14:
		return "BIAS_SENSN: ";
		break;
	case 15:
		return "LOFF_SENSP: ";
		break;
	case 16:
		return "LOFF_SENSN: ";
		break;
	case 17:
		return "LOFF_FLIP: ";
		break;
	case 18:
		return "LOFF_STATP: ";
		break;
	case 19:
		return "LOFF_STATN: ";
		break;
	case 20:
		return "GPIO: ";
		break;
	case 21:
		return "MISC1: ";
		break;
	case 22:
		return "MISC2: ";
		break;
	case 23:
		return "CONFIG4: ";
		break;
	default:
		return "ERROR_REG_NUM";
		break;
 }
}

void CommunicatorPrintMenu(void){
	if(!ADS1299IsRunning()) {
		UARTSendString(COMM_UART, (uint8_t*)"\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"------------------\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"  Command list:\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"------------------\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send 'b' to start data stream\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send 's' to stop data stream\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"use 1,2,3,4,5,6,7,8 to turn OFF channels\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"use !,@,#,$,%,^,&,* to turn ON channels\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"in 16chan, use q,w,e,r,t,y,u,i to turn OFF channels\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"in 16chan, use Q,W,E,R,T,Y,U,I to turn ON channels\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send 'd' to set channels to default\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send 'D' to print channels default\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send '?' to print all registers\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send 'v' to initialize board\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send 0,-,=,p,[,] to enable test signals\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send 'z (CHANNEL, PCHAN, NCHAN) Z' to start impedance test (see User Manual)\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send 'x (CHANNEL, POWER_DOWN, GAIN_SET, INPUT_TYPE_SET, BIAS_SET, SRB2_SET, SRB1_SET) X'  for channel setting (see User Manual)\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send '~3' for 2kHz sample rate\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send '~4' for 1kHz  sample rate\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send '~5' for 500Hz sample rate\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send '~6' for 250Hz sample rate\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send '/0' for board mode default\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send '/1' for board mode debug\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send '/2' for board mode analog\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send '/3' for board mode digital\r\n");
		UARTSendString(COMM_UART, (uint8_t*)"send '/4' for board mode marker\r\n");
	}
}

uint8_t CommunicatorGetChannelNumber(uint8_t n)
{
	if(n>'0' && n<'9')
	{
		n = n - '1';
	}
	else {
		switch(n)
		{
			case 'Q':
				n = 0x08;
				break;
			case 'W':
				n = 0x09;
				break;
			case 'E':
				n = 0x0A;
				break;
			case 'R':
				n = 0x0B;
				break;
			case 'T':
				n = 0x0C;
				break;
			case 'Y':
				n = 0x0D;
				break;
			case 'U':
				n = 0x0E;
				break;
			case 'I':
				n = 0x0F;
				break;
			default:
				n = 0;
				break;
		}
	}
	return n;
}

/*==================[external functions definition]==========================*/

/*==================[end of file]============================================*/
