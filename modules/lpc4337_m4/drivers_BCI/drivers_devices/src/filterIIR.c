/* Copyright 2020,
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "filterIIR.h"
#include "arm_const_structs.h"


/*==================[macros and definitions]=================================*/
#define FILTER_NUMSTAGES_BP 1
#define FILTER_NUMSTAGES_BS 2

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
const float32_t IIR_filter_coefficients_1[5] = //Buterworth bandpass 8 to 30
{
		0.22102179085279855, 0, -0.22102179085279855, 1.4383013998046956, -0.5579652749772579// b0, b1, b2, a1, a2
};

const float32_t IIR_filter_coefficients_n[10] = //Buterworth bandstopp 48 to 52
{
    0.9313788581229819, -0.5763517479909793, 0.9313788581229819, 0.662433737017782, -0.9321491306907809,// b0, b1, b2, a1, a2
    1, -0.6188155796799031, 1, 0.5312749131399023, -0.9306151829469796// b0, b1, b2, a1, a2

};
float32_t IIR_filter_state_BP[sizeof(IIR_filter_coefficients_1)-FILTER_NUMSTAGES_BP];
float32_t IIR_filter_state_BS[sizeof(IIR_filter_coefficients_n)-FILTER_NUMSTAGES_BS];
arm_biquad_casd_df1_inst_f32 IIR_filter_instance_BP;
arm_biquad_casd_df1_inst_f32 IIR_filter_instance_BS;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

 void FilterIIRInit(void)
{
	 arm_biquad_cascade_df1_init_f32(&IIR_filter_instance_BP, FILTER_NUMSTAGES_BP, (float32_t*)IIR_filter_coefficients_1, IIR_filter_state_BP);
	 arm_biquad_cascade_df1_init_f32(&IIR_filter_instance_BS, FILTER_NUMSTAGES_BS, (float32_t*)IIR_filter_coefficients_n, IIR_filter_state_BS);
	 FilterIIRReset();
}

 void FilterIIRReset(void)
{
	 memset( IIR_filter_state_BP, 0, sizeof( IIR_filter_state_BP ) ); // Reset state to 0
	 memset( IIR_filter_state_BS, 0, sizeof( IIR_filter_state_BS ) ); // Reset state to 0
}

 void FilterIIR(float32_t * p_input, float32_t * p_output)
{
	 float32_t aux_buffer[FILTER_BLOCKSIZE];
	 arm_biquad_cascade_df1_f32(&IIR_filter_instance_BP, p_input, aux_buffer, FILTER_BLOCKSIZE);
	 arm_biquad_cascade_df1_f32(&IIR_filter_instance_BS, aux_buffer, p_output, FILTER_BLOCKSIZE);
}

 /*==================[end of file]============================================*/


