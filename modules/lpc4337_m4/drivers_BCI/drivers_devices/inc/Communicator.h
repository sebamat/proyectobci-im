/* Copyright 2021,
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H

/*
 * Initials     Name
 * ---------------------------
 *  SM			Sebastian Mateos
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210406 v0.1 initials initial version by Sebastian Mateos
 */


/*==================[inclusions]=============================================*/
#include <stdint.h>
#include <stdbool.h>
#include "uart.h"

/*==================[macros]=================================================*/
#define COMM_UART UART_USB
#define COMM_BAUD_RATE 115200
#define COMM_DEBUG_UART UART_RS232
#define COMM_DEBUG_BAUD_RATE 460800

/*==================[typedef]================================================*/
/** @brief Defines the different modes of the board.
 *
 */
typedef enum {
	COMM_BOARD_MODE_DEFAULT,
	COMM_BOARD_MODE_DEBUG,
	COMM_BOARD_MODE_ANALOG,
	COMM_BOARD_MODE_DIGITAL,
	COMM_BOARD_MODE_MARKER,
	COMM_BOARD_MODE_END
}communicator_board_mode_t;

/*==================[external data declaration]==============================*/
/**
 * @brief Function for init communicator module.
 *  Initializes UART used in COMM and DEBUG. Uses systick for timing. Sends MSJ init.
 */
void CommunicatorInit(void);

/**
 * @brief Function for interpreting commands
 * @param token Command to be decoded and interpreted
 * @return TRUE if the command can be processed correctly
 */
bool CommunicatorProccessCommand(uint8_t token);

#endif /* COMMUNICATOR_H */
