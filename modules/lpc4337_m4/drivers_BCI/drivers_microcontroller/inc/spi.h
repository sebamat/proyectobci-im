/* Copyright 2020,
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef SPI_H
#define SPI_H

/** @addtogroup spi
 *  @{
 *  @file spi.h
 *  @brief Driver Bare Metal para la interfaz spi de la EDU_CIAA
 *
 *
 *  @section changeLog Historial de modificaciones (la mas reciente arriba)
 *
 *  20200420 v0.1 version inicial SM
 *
 *  Iniciales  |     Nombre
 * :----------:|:----------------
 *  SM		   | Sebastian Mateos
 *
 *  @date 20/04/2020
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>
#include <stdbool.h>
#include "gpio.h"

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/** available SPI ports */
typedef enum SPIPort {
	SPI0, /**< SPI0 interface */
	SPI1  /**< SPI1 interface */
}SPI_t;

/**
 * @brief SPI phase and polarity modes
 */
typedef enum {
	MODE0,		/*!< phase = 0 and polarity = 0 */
	MODE1,		/*!< phase = 1 and polarity = 0 */
	MODE2,		/*!< phase = 0 and polarity = 1 */
	MODE3		/*!< phase = 1 and polarity = 1 */
} spi_mode_t;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** initialize SPI port
 *
 * @param p          port number, @ref SPI0 or @ref SPI1
*  @param mode Transfer mode
 * @param clock_rate SPI clock rate
 */
int32_t SPIInit(SPI_t p, spi_mode_t mode, uint32_t clock_rate);

/** transmit and receive data through SPI port (full-duplex)
 *
 * @param p     port number, @ref SPI0 or @ref SPI1
 * @param ssel  digital output to use as slave select, user must previously configure
 * it with @ref picoConfigDigitalOutput and initial value 1 (logic high). If ssel is
 * set as @ref PICOPIN_COUNT, user must take control of SSEL signal.
 * @param txbuf transmit buffer
 * @param rxbuf receive buffer
 * @param len   transfer length in bytes
 * @return transfer status, -1 on error
 */
int32_t SPITransferBuffer(SPI_t p, gpio_t ssel, uint8_t * txbuf, uint8_t * rxbuf, int32_t len);


/** fast transmit and receive a single data through SPI port (full-duplex)
 *
 * @param p     port number, @ref SPI0 or @ref SPI1
 * @param tx transmit data
 * @return recived data
 */
uint8_t SPITransferFast(SPI_t p,  uint8_t tx);

/*==================[cplusplus]==============================================*/

/** @} */

#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/
#endif /* SPI_H */
