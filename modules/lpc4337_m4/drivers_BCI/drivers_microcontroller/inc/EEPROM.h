/* Copyright 2019,
 * Sebastian Mateos
 * sebastianantoniomateos@gmail.com
 * Cátedra Electrónica Programable
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @addtogroup EEPROM
 *  @{
 *  @file EEPROM.h
 *  @brief Driver Bare Metal para manejar la memoria EEPROM de la EDU-CIAA.
 *
 *
 *  @section changeLog Historial de modificaciones (la mas reciente arriba)
 *
 *  20190328 v0.1 version inicial SM
 *
 *  Iniciales  |     Nombre
 * :----------:|:----------------
 *  SM		   | Sebastian Mateos
 *
 *  @date 28/03/2019
 */

#ifndef EEPROM_H
#define EEPROM_H


/*==================[inclusions]=============================================*/
#include <stdint.h>
#include <stdbool.h>

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/
/** @fn void EEPROMInit(void)
 * @brief Inicializacion del driver
 */
void EEPROMInit(void);

/** @fn int32_t EEPROMWriteByte(uint32_t addr,uint8_t value)
 * @brief Escribe un dato en memoria EEPROM
 * @param[in] addr Direccion dentro de la memoria eeprom donde se desea guardar el dato.
 * @param[in] value Dato que se desea guardar.
 */
uint8_t EEPROMWriteByte(uint32_t addr, uint8_t value);

/** @fn int32_t EEPROMReadByte(uint32_t addr)
 * @brief Lee un dato de memoria EEPROM
 * @param[in] addr Direccion de la memoria eeprom del dato que se desea leer.
 */
uint8_t EEPROMReadByte(uint32_t addr);

/*==================[end of file]============================================*/
#endif /* EEPROM_H */

/** @}*/

