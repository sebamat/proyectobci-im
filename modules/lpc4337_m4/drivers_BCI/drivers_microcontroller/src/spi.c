/* Copyright 2020,
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

#include "spi.h"
#include "chip.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/
#define SPI_CLK1_PORT  		0xF
#define SPI_CLK1_PIN   		0x4
#define SPI_MISO1_PORT 	0x1
#define SPI_MISO1_PIN   	0x3
#define SPI_MOSI1_PORT  	0x1
#define SPI_MOSI1_PIN   	0x4

#define MASTER_EN		1

/*==================[internal data declaration]==============================*/
static Chip_SSP_DATA_SETUP_T ssp1_data;		/*!< Data setup structure */

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int32_t SPIInit(SPI_t p, spi_mode_t mode, uint32_t clock_rate)
{
	int32_t ret_value = -1;
	switch(p)
	{
	case SPI0:
		 ret_value = -1;
		break;
	case SPI1:
		Chip_SCU_PinMux(SPI_MOSI1_PORT, SPI_MOSI1_PIN,
				SCU_MODE_PULLUP, SCU_MODE_FUNC5); /* SPI1_MOSI */
		Chip_SCU_PinMux(SPI_CLK1_PORT, SPI_CLK1_PIN,
				SCU_MODE_PULLUP, SCU_MODE_FUNC0); /* SPI1_SCK */
		Chip_SCU_PinMux(SPI_MISO1_PORT, SPI_MISO1_PIN,
				SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS, SCU_MODE_FUNC5); /* SPI1_MISO */

    	/* Initialize SPI controller */
    	Chip_SSP_Init(LPC_SSP1);

    	/* Call to initialize first SPI controller for mode1, master mode,
    	   MSB first */
		switch(mode)
		{
		case MODE0:
			Chip_SSP_SetFormat(LPC_SSP1, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_MODE0);
			break;

		case MODE1:
			Chip_SSP_SetFormat(LPC_SSP1, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_MODE1);
			break;

		case MODE2:
			Chip_SSP_SetFormat(LPC_SSP1, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_MODE2);
			break;

		case MODE3:
			Chip_SSP_SetFormat(LPC_SSP1, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_MODE3);
			break;
		}
    	Chip_SSP_Enable(LPC_SSP1);
    	Chip_SSP_SetMaster(LPC_SSP1, MASTER_EN);
    	Chip_SSP_SetBitRate(LPC_SSP1, clock_rate);
    	ret_value = 1;
    	break;
	}
	return ret_value;
}

int32_t SPITransferBuffer(SPI_t p, gpio_t ssel, uint8_t * txbuf, uint8_t * rxbuf, int32_t len)
{
	int32_t ret_value = -1;
	switch(p)
	{
		case SPI1:
			/* If CS controlled by driver, activate CS */
			if (ssel != GPIO_NULL)
			{
				GPIOOff(ssel);
			}
			/* Initialize data setup structure */
			ssp1_data.tx_data = txbuf;
			ssp1_data.tx_cnt = 0;
			ssp1_data.rx_data = rxbuf;
			ssp1_data.rx_cnt = 0;
			ssp1_data.length = len;
			ret_value = Chip_SSP_RWFrames_Blocking(LPC_SSP1, &ssp1_data);

			/* If CS controlled by driver, deactivate CS */
			if (ssel != GPIO_NULL)
			{
				GPIOOn(ssel);
			}
			break;
		default:
			break;
		}
	return ret_value;
	}

uint8_t SPITransferFast(SPI_t p,  uint8_t tx)
{
	uint8_t rx = 0;
	if (p == SPI1) {
		LPC_SSP1->DR = tx & 0xFFFF;
		while (LPC_SSP1->SR & 0x10 ) { ; }
		rx = LPC_SSP1->DR;
	}
	else {
		rx = 0;
	}
	return rx;
}

/*==================[end of file]============================================*/
