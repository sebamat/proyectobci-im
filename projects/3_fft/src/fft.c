/* Copyright 2020,
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Initials     Name
 * ---------------------------
 * SM Sebastian Mateos
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20200602 v0.0.1 SM initial version
 */

/*!\mainpage
*
* \section genDesc Descripción de la aplicacion
*	Sistema de adquisición monocanal de una señal con filtrado pasabanda entre 8 y 30Hz y notch en 50Hz.
*	El filtro se diseño utilizando la herramienta https://www.micromodeler.com/dsp/.
* 	Utiliza el canal 2 en modo diferencial para adquirir, a una frecuencia de muestreo de 250Hz, los datos en 24bits.
* 	Estos datos son enviados por UART USB en un formato compatible con el hardware de OpenBCI. Dando posibilidad de utilizarlo
*	con cualquier software que implemente este tipo de datos.
*
* ---
*
* \subsection subSec01 Funcionamiento
*	Parpadeo del led azul RGB a una frecuencia de 1Hz y envío de los datos convertidos y filtrados cada 4ms por UART USB.
*	Para utilizar en la EDU CIAA NXP, al ser un poncho para dicha placa consultar la bibligrfía en la página
*	https://ponchodebiopotenciales.wordpress.com/ para más información
*
*/

/*==================[inclusions]=============================================*/
#include "fft.h"

#include "ADS1299.h"
#include "board.h"
#include "led.h"
#include "uart.h"
#include "systick.h"
#include "delay.h"
#include "filterIIR.h"
#include "arm_math.h"
#include "arm_const_structs.h"

/*==================[macros and definitions]=================================*/
#define IFFT_FLAG 0
#define DO_BIT_REVERSE 1
#define WINDOW_WIDTH 128

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/
//void SystickInt(void);
void ReadData(void);
void ConfigADS(void);

/*==================[internal data definition]===============================*/
bool new_data = false, new_window = false;
uint8_t state[3];
int32_t channel_data[8];
float32_t channel2_window[FILTER_BLOCKSIZE];

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
/*/fn void SysInit(void)
 * \brief Inicializacion principal
 *
 */
void SysInit(void)
{
	Board_Init();
	SystemCoreClockUpdate();
	LedsInit();
	fpuInit();
	ADS1299Init();
//    SystickInit(500, SystickInt);
    FilterIIRInit();
}

//void SystickInt(void)
//{
//	LedToggle(LED_RGB_B);
//}

void ReadData(void)
{
	static uint8_t i_sample = 0;
	new_data = true;
	if(i_sample<FILTER_BLOCKSIZE-1)
	{
		channel2_window[i_sample] =  (float32_t)(channel_data[1] * 0.000000298);
		i_sample++;
	}
	else
	{
		channel2_window[i_sample] =  (float32_t)(channel_data[1] * 0.000000298);
		i_sample=0;
		new_window = true;
	}
}

void ConfigADS(void)
{
	/* Solo canal 2 */
	ADS1299SetChannelsToDefaultConfigForECG();
	ADS1299ChangeChannelPGAGain(ADS1299_CHANNEL2, ADS1299_GAIN01);

	/* Datos convertidos */
	ADS1299ActivateInt(ReadData, channel_data, state);
//	ADS1299ActivateUARTStreaming();

	ADS1299StartStreaming();
}

/*==================[external functions definition]==========================*/
int main(void)
{
	uint8_t i_uart=0, i_window=0, i, buf;
	uint8_t uart_buffer[24] = {};
	int32_t aux_sample_int32;

	float32_t IIR_input_buffer[FILTER_BLOCKSIZE], IIR_output_buffer[FILTER_BLOCKSIZE];

	float32_t signal[WINDOW_WIDTH];
	float32_t fft_input[2*WINDOW_WIDTH] = {};
	float32_t fft_output[WINDOW_WIDTH] = {};

	SysInit();
	ConfigADS();

	while(1)
	{
		if(new_data)
		{
//			aux_sample_int32 = (int32_t) (IIR_input_buffer[i_uart] / 0.000000298);
//			uart_buffer[0] = (uint8_t) (aux_sample_int32 >>16);
//			uart_buffer[1] = (uint8_t) (aux_sample_int32 >>8);
//			uart_buffer[2] = (uint8_t) aux_sample_int32;
//			aux_sample_int32 = (int32_t) (IIR_output_buffer[i_uart] / 0.000000298);
//			uart_buffer[3] = (uint8_t) (aux_sample_int32 >>16);
//			uart_buffer[4] = (uint8_t) (aux_sample_int32 >>8);
//			uart_buffer[5] = (uint8_t) aux_sample_int32;
//			ADS1299SendUART(uart_buffer);
//			i_uart++;

			if(new_window)
			{
				LedOn(LED_RGB_B);
				for(i=0 ; i<FILTER_BLOCKSIZE ; i++)
				{
					IIR_input_buffer[i] = channel2_window[i];
				}
				FilterIIR(IIR_input_buffer, IIR_output_buffer);

				if(i_window<WINDOW_WIDTH)
				{
					for(i=0 ; i<FILTER_BLOCKSIZE ; i++)
					{
						signal[i_window] = IIR_output_buffer[i];
						i_window++;
					}
				}
				else
				{
					for(i=0 ; i<(WINDOW_WIDTH-FILTER_BLOCKSIZE) ; i++)
					{
						signal[i] = signal[i+FILTER_BLOCKSIZE];
						fft_input[i*2] = signal[i];
						fft_input[i*2+1] = 0;
					}
					for(i=0 ; i<FILTER_BLOCKSIZE ; i++)
					{
						signal[i+(WINDOW_WIDTH-FILTER_BLOCKSIZE)] = IIR_output_buffer[i];
						fft_input[i*2+(WINDOW_WIDTH-FILTER_BLOCKSIZE)*2] = IIR_output_buffer[i];
						fft_input[i*2+1+(WINDOW_WIDTH-FILTER_BLOCKSIZE)*2] = 0;
					}

					  /* Process the data through the CFFT/CIFFT module */
					arm_cfft_f32(&arm_cfft_sR_f32_len128, fft_input, IFFT_FLAG, DO_BIT_REVERSE);
					arm_cmplx_mag_f32(fft_input, fft_output, WINDOW_WIDTH);
					LedOff(LED_RGB_B);

					buf = 0x0A;
					UARTSendByte(UART_USB, &buf);
					buf = 0x0D;
					UARTSendByte(UART_USB, &buf);
					for(i=0 ; i<12 ; i++)
					{
						buf = ((uint32_t)(fft_output[i]*10)& 0xFF000000)>>24;
						UARTSendByte(UART_USB, &buf);
						buf = ((uint32_t)(fft_output[i]*10)& 0x00FF0000)>>16;
						UARTSendByte(UART_USB, &buf);
						buf = ((uint32_t)(fft_output[i]*10)& 0x0000FF00)>>8;
						UARTSendByte(UART_USB, &buf);
						buf = ((uint32_t)(fft_output[i]*10)& 0x000000FF);
						UARTSendByte(UART_USB, &buf);
					}

//					for(i=0 ; i<WINDOW_WIDTH/2 ; i++)
//					{
//						/* Formato de recepcion de señales de la app para celulares Bluetooth Electronics */
//
//						UARTSendString(UART_RS232, "*HX");
//						UARTSendString(UART_RS232, Itoa((i), 10));
//						UARTSendString(UART_RS232, "Y");
//						UARTSendString(UART_RS232, Itoa((uint32_t)(fft_output[i]*10), 10));
//						UARTSendString(UART_RS232, "*");
//					}
				}
				new_window = false;
				i_uart = 0;
			}
			new_data = false;
		}
	}
	return 0;
}
/*==================[end of file]============================================*/
