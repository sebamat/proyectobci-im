/* Copyright 2020,
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Initials     Name
 * ---------------------------
 * SM Sebastian Mateos
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20200602 v0.0.1 SM initial version
 */

/*!\mainpage
*
* \section genDesc Descripción de la aplicacion
*	Sistema de adquisición monocanal de una señal de ECG. Utiliza el canal 2 en modo diferencial para adquirir, a una frecuencia de muestreo de 250Hz,
*	los datos en 24bits. Estos datos son enviados por UART USB en un formato compatible con el hardware de OpenBCI. Dando posibilidad de utilizarlo
*	con cualquier software que implemente este tipo de datos.
*
* ---
*
* \subsection subSec01 Funcionamiento
*	Parpadeo del led azul RGB a una frecuencia de 1Hz y envío de los datos convertidos cada 4ms por UART USB.
*	Para utilizar en la EDU CIAA NXP, al ser un poncho para dicha placa consultar la bibligrfía en la página
*	https://ponchodebiopotenciales.wordpress.com/ para más información
*
*/

/*==================[inclusions]=============================================*/
#include "adquisition.h"

#include "ADS1299.h"
#include "board.h"
#include "led.h"
#include "uart.h"
#include "systick.h"
#include "delay.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/
void SystickInt(void);
void ReadData(void);
void ConfigADS(void);

/*==================[internal data definition]===============================*/
bool new_data = false;
int32_t channel_data[8];
uint8_t state[3];

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
/*/fn void SysInit(void)
 * \brief Inicializacion principal
 *
 */
void SysInit(void)
{
	Board_Init();
	SystemCoreClockUpdate();
	LedsInit();
	ADS1299Init();
    SystickInit(500, SystickInt);
}

void SystickInt(void)
{
	LedToggle(LED_RGB_B);
}

void ReadData(void)
{
	new_data = true;
}

void ConfigADS(void)
{
	/* Configura el canal 2 en modo diferencial, frecuencia de muestreo de 250Hz y el driver de pierna derecha */
	ADS1299SetChannelsToDefaultConfigForECG();
	/* Cambia la ganancia a 1 para poder utilizarlo con un generador de funciones y que no sature el ADS */
//	ADS1299ChangeChannelPGAGain(ADS1299_CHANNEL2, ADS1299_GAIN01);

	/* Datos convertidos */
	/* Indica a traves de la funcion de interrupcion ReadData que hay un dato nuevo en el vector channel_data y state.
	 * Los datos son de 24bits en complemento a 2.
	 */
	ADS1299ActivateInt(ReadData, channel_data, state);

	/* Comienzo de la conversión continua */
	ADS1299StartStreaming();
}

/*==================[external functions definition]==========================*/
int main(void)
{
	uint8_t uart_buffer[24] = {};

	SysInit();
	ConfigADS();

	while(1)
	{
		if(new_data)
		{
			/* Envio del canal 2 a traves de la UART USB en formato de datos de OpenBCI */
			uart_buffer[0] = (uint8_t) (channel_data[1]>>16);
			uart_buffer[1] = (uint8_t) (channel_data[1]>>8);
			uart_buffer[2] = (uint8_t) (channel_data[1]);
			ADS1299SendUART(uart_buffer);
			new_data = false;
		}
	}
	return 0;
}
/*==================[end of file]============================================*/
