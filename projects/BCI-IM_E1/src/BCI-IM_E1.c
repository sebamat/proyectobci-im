/* Copyright 2019,
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Initials     Name
 * ---------------------------
 * SM		Sebastian Mateos
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20190404 v0.1 SM initial version
 */

/** @mainpage Interfaz cerebro computadora por imagineria motora
*
* @section genDesc Descripción de la aplicacion
* Este sistema implementa una interfaz cerebro computadora
*
* ---
*
* @subsection subSec01 Funcionamiento
*
*/

/*==================[inclusions]=============================================*/
#include "BCI-IM_E1.h"

#include <stdint.h>

#include "ADS1299.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "uart.h"
#include "EEPROM.h"
#include "fpu_init.h"
#include "filterIIR.h"
#include "systick.h"

#define ARM_MATH_CM4
#define __FPU_PRESENT 1
#include <arm_math.h>
#include "arm_const_structs.h"

/*==================[macros and definitions]=================================*/
#define CHANNELS 5
#define WINDOWS_SHIFT 8
#define WINDOW_WIDTH 128

#define FILTER_NUMSTAGES_BP 1
#define FILTER_NUMSTAGES_BS 2

#define IFFT_FLAG 0
#define DO_BIT_REVERSE 1

#define COEFFS 12
#define IMG_THRESHOLD 0
#define COEFF_PROM 1
#define NUM_RIGHT 3
#define TRIALS 15

/*==================[internal data definition]===============================*/
bool new_data = false; /**< Booleano que indica si hay un nuevo dato para procesar */
bool new_window = false; /**< Booleano que indica si hay una nueva ventana */
uint8_t i_shift = 255; /**< Indice para recorrer el desplazamiento de la ventana */
uint8_t mode = 0; /**< Indica en que modo de funcionamiento esta el sistema */
uint8_t systick_cont = 0; /**< Indica en que modo de funcionamiento esta el sistema */
float32_t input_buffer[CHANNELS][WINDOWS_SHIFT];/**< Vector de almacenamiento de los valores de entrada*/
int32_t channels[8]; /**< Pointer to an external data buffer*/
uint8_t states[3]; /**< Pointer to an external state data arrow*/

const float32_t IIR_filter_coefficients_BP[5] = /**< Coeficientes de un filtro IIR Buterworth pasabanda de 8 a 30Hz */
{
		0.22102179085279855, 0, -0.22102179085279855, 1.4383013998046956, -0.5579652749772579// b0, b1, b2, a1, a2
};

const float32_t IIR_filter_coefficients_BS[10] = /**< Coeficientes de un filtro IIR Buterworth rechaza banda de 48 a 52Hz */
{
    0.9313788581229819, -0.5763517479909793, 0.9313788581229819, 0.662433737017782, -0.9321491306907809,// b0, b1, b2, a1, a2
    1, -0.6188155796799031, 1, 0.5312749131399023, -0.9306151829469796// b0, b1, b2, a1, a2

};

const float32_t intention[COEFFS] = {'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I'};
const float32_t repose[COEFFS] = {'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R'};

/*==================[internal functions declaration]=========================*/
/** @brief Inicializacion principal
 *
 * Inicializacion de:
 * + Reloj del sistema
 * + FPU
 * + Teclas y sus interrupciones
 * + Leds
 * + ADS1299
 * + UART USB
 * + Memoria EEPROM
 * + Modo del sistema
 *
 */
void SysInit(void);

/** @brief Configuracion del bloque de adquisicion
 * Configura 5 canales del poncho de biopotenciales,
 * canales 2,3,5,6,7 con una ganancia de 24 y el driver de pierna derecha
 */
void ConfigADS(void);

/** @brief Atiende la interrupcion del bloque de adquisicion, lee los nuevos datos
 *	Carga 5 vectores con los 5 canales de registro convirtiendo los datos de complemento a 2 de 24 bits
 *	a flotante, tambien administra el ingreso de datos por ventanas de tiempo
 */
void ReadData(void);

/** @brief Atiende la interrupcion de la tecla 1
 * Cambia a modo de funcionamiento 1, modo de funcionamiento "on line" de la BCI
 */
void Tec1Int(void);

/** @brief Atiende la interrupcion de la tecla 2
 * Cambia a modo de funcionamiento 2, modo de visualizacion de los datos en tiempo real
 */
void Tec2Int(void);

/** @brief Atiende la interrupcion de la tecla 3
 * Cambia a modo de funcionamiento 3, modo de visualizacion de la FFT en tiempo real
 */
void Tec3Int(void);

/** @brief Atiende la interrupcion de la tecla 4
 * Cambia a modo de funcionamiento 4, modo de carga del vector de pesos del clasificador
 */
void Tec4Int(void);

/** @brief Envia los datos en tiempo real por el puerto serie
 * Envia 1 canal con el resultado del filtro laplaciano y los 5 canales luego de los filtrados frecuenciales
 */
void SendData(float32_t * laplaciano, float32_t output_buffer[CHANNELS][WINDOWS_SHIFT]);

/** @brief Envia la FFT en tiempo real por el puerto serie
 * Si se mantiene presionada la tecla 3 se envian los datos para ser almacenados
 * y sino se envian para visualizar la FFT
 */
void SendCoefs(float32_t * coeffs);

/** @brief Carga el vector de pesos del clasificador, almacenado en memoria EEPROM, en memoria RAM
 *
 */
void ChargeLDA(float32_t * weighing);

/** @brief Recibe el vector de pesos del clasificador y lo almacena en memoria RAM y EEPROM
 *
 */
void ConfigLDA(float32_t * weighing);

/** @brief Funcion de interrupcion del systick periodica cada 1 seg
 *
 */
void SystickInt(void);

/** @brief Funcion que realiza las indicaciones luminosas al usuario para realizar el entrenamiento
 *
 */
void TrainingManager(void);

/*==================[internal functions definition]==========================*/
int main(void)
{

	uint8_t i;/**< Indice para utilizar en los ciclos "for" */
	uint8_t i_window_init = 0; /**< Indice de inicializacion de la ventana de analisis de la señal */
	uint8_t i_coeff_prom = 0; /**< Indice de almacenamiento para realizar el promedio de los vectores de caracteristicas */
	uint8_t rights = 0; /**< Cuenta el numero de aciertos que lleva la BCI */
	float32_t imagging = 0; /**< Variable utilizada por el clasificador */
	float32_t power; /**< Variable para el calculo de potencia espectral */
	float32_t laplaciano[WINDOWS_SHIFT]; /**< Vector de almacenamiento del filtrado laplaciano*/
	float32_t fft_input[2*WINDOW_WIDTH], fft_output[WINDOW_WIDTH];  /**< Vectores de entrada y salida de la FFT*/
	float32_t signal[WINDOW_WIDTH]; /**< Vector de almacenamiento de las ventanas de la señal*/
	float32_t coeffs[COEFF_PROM][COEFFS]; /**< Vector de entrada de coeficientes*/
	float32_t weighing[COEFFS]; /**< Vector de almacenamiento de los pesos de los coeficientes*/
	float32_t systick_old = 0; /**< Variable utilizada por el entrenador */

	/* Buffers de salida del filtro IIR */
	float32_t aux_buffer[WINDOWS_SHIFT];
	float32_t output_buffer[CHANNELS][WINDOWS_SHIFT];

	/* Funcion de inicializacion del sistema */
	SysInit();

	for(i=0 ; i<CHANNELS ; i++)
		{
			/* Funciones de inicializacion del filtro Pasa-Banda */
			FilterIIRInit();

			/* Funciones de inicializacion del filtro Rechaza-Banda */
			FilterIIRInit();
		}

	/* Funcion de inicializacion del ADS1299 */
	ConfigADS();

	/* Funcion que carga el vector de pesos del LDA almacenado en memoria EEPROM */
	ChargeLDA(weighing);

	while(1)
	{
		if(new_window)
		{
			GPIOOn(GPIO_T_COL0);

			/* Filtrado Pasa-Banda y Rechaza-Banda de cada bloque de WINDOWS_SHIFT */
			for(i=0 ; i<CHANNELS ; i++)
					{
						FilterIIR(input_buffer[i], output_buffer[i]);
					}

			/* Filtrado Laplaciano */
			for(i=0 ; i<WINDOWS_SHIFT ; i++)
				laplaciano[i] = output_buffer[0][i] - (output_buffer[1][i] + output_buffer[2][i] + output_buffer[3][i] + output_buffer[4][i])/4;

			/* Desplazamiento de ventana y agregado de valores nuevos*/
			if(i_window_init<WINDOW_WIDTH)
			{
				for(i=0 ; i<WINDOWS_SHIFT ; i++)
				{
					signal[i_window_init] = laplaciano[i];
					i_window_init++;
				}
			}
			else
			{
				for(i=0 ; i<(WINDOW_WIDTH-WINDOWS_SHIFT) ; i++)
				{
					signal[i] = signal[i+WINDOWS_SHIFT];
					fft_input[i*2] = signal[i];
					fft_input[i*2+1] = 0;
				}
				for(i=0 ; i<WINDOWS_SHIFT ; i++)
				{
					signal[i+(WINDOW_WIDTH-WINDOWS_SHIFT)] = laplaciano[i];
					fft_input[i*2+(WINDOW_WIDTH-WINDOWS_SHIFT)*2] = laplaciano[i];
					fft_input[i*2+1+(WINDOW_WIDTH-WINDOWS_SHIFT)*2] = 0;
				}

				/* Procesamiento para realizar la FFT */
				arm_cfft_f32(&arm_cfft_sR_f32_len128, fft_input, IFFT_FLAG, DO_BIT_REVERSE);
				arm_cmplx_mag_f32(fft_input, fft_output, WINDOW_WIDTH);

//				/* Calculo de la potencia total del espectro */
//				power = 0;
//				for(i=0 ; i<COEFFS ; i++)
//					power += fft_output[i+4];
//
//				/* Normalizacion del vector de caracteristicas */
//				for(i=0 ; i<COEFFS ; i++)
//					coeffs[i_coeff_prom][i] = fft_output[i+4]/power;

				for(i=0 ; i<COEFFS ; i++)
					coeffs[i_coeff_prom][i] = fft_output[i+4];

				/* Almacenamiento de COEF_PROM vectores de caracteristicas */
				i_coeff_prom++;
				if(i_coeff_prom >= COEFF_PROM)
				{
					/* Promedio de COEFF_PROM vectores de coeficientes */
					for(i=0 ; i<COEFFS ; i++)
					{
						for(i_coeff_prom=1 ; i_coeff_prom<COEFF_PROM ; i_coeff_prom++)
							coeffs[0][i] += coeffs[i_coeff_prom][i];
						coeffs[0][i] /= COEFF_PROM;
					}
					i_coeff_prom = 0;

					/* Calculo de la potencia total del espectro */
					power = 0;
					for(i=0 ; i<COEFFS ; i++)
						power += coeffs[0][i];

					/* Normalizacion del vector de caracteristicas */
					for(i=0 ; i<COEFFS ; i++)
						coeffs[0][i] /= power;

					/* Clasificador de IM */
					if(mode & (1<<0))
					{
						arm_dot_prod_f32(coeffs[0], weighing, COEFFS, &imagging);
						if(imagging>IMG_THRESHOLD)
						{
							if(rights >= NUM_RIGHT)
							{
								LedOn(LED_RGB_G);
								LedOff(LED_RGB_B);
							}
							else
								rights++;
						}
						else
						{
							if(rights > 0)
								rights--;
							else
							{
								LedOn(LED_RGB_B);
								LedOff(LED_RGB_G);
							}
						}
					}

					/* Envio de coeficiontes a la PC */
					if(mode & (1<<2))
					{
						SendCoefs(coeffs[0]);

						/* Descomentar para que por cada pulso de SWITCH_3 se muestre solo 1 FFT */
	//					mode = 1;
	//					LedsMask(LED_B);
					}
				}
			}
			GPIOOff(GPIO_T_COL0);
			new_window = false;
		}
		if(new_data && !new_window)
		{
			/* Visualizacion de datos filtrados */
			if(mode & (1<<1))
				SendData(laplaciano, output_buffer);
			new_data = false;
		}
		 /* Carga en memoria RAM y EEPROM el vector de pesos */
		if(mode & (1<<3))
		{
			ConfigLDA(weighing);
//			mode = (1<<0);
//			LedsMask(LED_B);
			mode &= ~(1<<3);
			LedOff(LED_3);
		}
		if(systick_cont != systick_old)
		{
			systick_old = systick_cont;
			TrainingManager();
		}
	}
	return 0;
}


void SysInit(void)
{
	Board_Init();
	fpuInit();
	LedsInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1, Tec1Int);
	SwitchActivInt(SWITCH_2, Tec2Int);
	SwitchActivInt(SWITCH_3, Tec3Int);
	SwitchActivInt(SWITCH_4, Tec4Int);
	ADS1299Init();
	EEPROMInit();
	mode = (1<<0);
	LedsMask(LED_RGB_B);

	GPIOInit(GPIO_T_COL0, GPIO_OUTPUT);
	GPIOOff(GPIO_T_COL0);
}

void ReadData(void)
{
	uint8_t i, j;
	static int32_t sample_int[CHANNELS];
	static int32_t new_buffer[CHANNELS][WINDOWS_SHIFT];
	new_data = true;

	/* Copia los datos de los canales que andan en el poncho */

	sample_int[0] = channels[2];
	sample_int[1] = channels[3];
	sample_int[2] = channels[5];
	sample_int[3] = channels[6];
	sample_int[4] = channels[7];

	/* Carga de muestras de cada canal en un buffer interno,
	 * y cuando se completa el bufer de WINDOWS_SHIFT muestras, se copia a los vectores de entrada
	 *  al filtro pasabanda
	 */
	i_shift++;
	if(i_shift<WINDOWS_SHIFT)
	{
		for(i=0 ; i<CHANNELS ; i++)
			new_buffer[i][i_shift] = sample_int[i];
	}
	else
	{
		for(i=0 ; i<CHANNELS ; i++)
			for(j=0 ; j<WINDOWS_SHIFT ; j++)
				input_buffer[i][j] = (float32_t)(new_buffer[i][j] * 0.000000298);
		i_shift = 0;
		for(i=0 ; i<CHANNELS ; i++)
			new_buffer[i][i_shift] = sample_int[i];
		new_window = true;
	}
}

void ConfigADS(void)
{
//	ADS1299SetChannelsToDefaultConfigForECG();
//	ADS1299ChangeChannelPGAGain(ADS1299_CHANNEL2, ADS1299_GAIN24);
	ADS1299SetChannelsToDefaultConfigForEEG();
//	ADS1299ChangeChannelPGAGain(ADS1299_CHANNEL2, ADS1299_GAIN01);
//	ADS1299ChangeChannelPGAGain(ADS1299_CHANNEL3, ADS1299_GAIN01);
//	ADS1299ChangeChannelPGAGain(ADS1299_CHANNEL5, ADS1299_GAIN01);
//	ADS1299ChangeChannelPGAGain(ADS1299_CHANNEL6, ADS1299_GAIN01);
//	ADS1299ChangeChannelPGAGain(ADS1299_CHANNEL7, ADS1299_GAIN01);
	ADS1299ActivateInt(ReadData, channels, states);
	ADS1299StartStreaming();
}

void Tec1Int(void)
{
//	mode ^= (1<<0);
//	LedToggle(LED_B);
	mode = (1<<0);
	LedsMask(LED_RGB_B);
	SystickDeinit();
}

void Tec2Int(void)
{
//	mode ^= (1<<1);
//	LedToggle(LED_1);
	mode = (1<<1);
	LedsMask(LED_1);
	SystickDeinit();
}

void Tec3Int(void)
{
//	mode ^= (1<<2);
//	LedToggle(LED_2);
	mode = (1<<2);
	LedsMask(LED_2);
	SystickInit(3000, SystickInt);
	systick_cont = 0;
}

void Tec4Int(void)
{
//	mode ^= (1<<3);
//	LedToggle(LED_3);
	mode = (1<<3);
	LedsMask(LED_3);
	SystickDeinit();
}

void SendData(float32_t * laplaciano, float32_t output_buffer[CHANNELS][WINDOWS_SHIFT])
{
	uint8_t uart_buffer[24], i;
	uart_buffer[0] = ((int32_t)(laplaciano[i_shift] / 0.000000298) & 0x00FF0000)>>16;
	uart_buffer[1] = ((int32_t)(laplaciano[i_shift] / 0.000000298) & 0x0000FF00)>>8;
	uart_buffer[2] = ((int32_t)(laplaciano[i_shift] / 0.000000298) & 0x000000FF);
	for(i=0 ; i<5 ; i++)
	{
		uart_buffer[(i*3)+3] = ((int32_t)(output_buffer[i][i_shift] / 0.000000298) & 0x00FF0000)>>16;
		uart_buffer[(i*3)+4] = ((int32_t)(output_buffer[i][i_shift] / 0.000000298) & 0x0000FF00)>>8;
		uart_buffer[(i*3)+5] = ((int32_t)(output_buffer[i][i_shift] / 0.000000298) & 0x000000FF);
	}
	ADS1299SendUART(uart_buffer);
}

void SendCoefs(float32_t * coeffs)
{
	uint8_t * transmit_pointer, i;
	UARTSendString(UART_USB, (uint8_t *)"sam");	/* Cabecera del vector de coeficientes */
//	UARTSendString(UART_USB, "sam");	/* Cabecera del vector de coeficientes */
	for(i=0 ; i<COEFFS ; i++)
	{
		transmit_pointer = (uint8_t *)(coeffs+i);
		UARTSendByte(UART_USB, transmit_pointer);
		UARTSendByte(UART_USB, transmit_pointer+1);
		UARTSendByte(UART_USB, transmit_pointer+2);
		UARTSendByte(UART_USB, transmit_pointer+3);
	}
}

void ChargeLDA(float32_t * weighing)
{
	uint8_t i, * point;
	if(EEPROMReadByte(0)=='I')
	{
		for(i=0 ; i<COEFFS ; i++)
		{
			point = (uint8_t *)(weighing+i);
//			point = (weighing+i);
			*point = EEPROMReadByte((i*4)+1);
			*(point+1) = EEPROMReadByte((i*4)+2);
			*(point+2) = EEPROMReadByte((i*4)+3);
			*(point+3) = EEPROMReadByte((i*4)+4);
		}
	}
}

void ConfigLDA(float32_t * weighing)
{
	uint8_t i, dato=0, * point;
	EEPROMWriteByte(0, 'I');
	for(i=0 ; i<COEFFS ; i++)
	{
//		point = (uint8_t *)(weighing+i); /* Es la forma correcta pero no anda */
		point = (weighing+i);
		while(!UARTReadByte(UART_USB, &dato)){}
		*point = dato;
		EEPROMWriteByte((i*4)+1, dato);
		while(!UARTReadByte(UART_USB, &dato)){}
		*(point+1) = dato;
		EEPROMWriteByte((i*4)+2, dato);
		while(!UARTReadByte(UART_USB, &dato)){}
		*(point+2) = dato;
		EEPROMWriteByte((i*4)+3, dato);
		while(!UARTReadByte(UART_USB, &dato)){}
		*(point+3) = dato;
		EEPROMWriteByte((i*4)+4, dato);
	}
}

void SystickInt(void)
{
	systick_cont++;
}

void TrainingManager(void)
{
	if(systick_cont >= (4+TRIALS*4))
	{
		LedsMask(LED_2);
		SystickDeinit();
	}
	else if(systick_cont >= 4)
	{
		if(systick_cont%4 == 0)
		{
			LedOn(LED_RGB_G);
			LedOff(LED_RGB_B);
			SendCoefs(intention);
		}
		else if(systick_cont%4 == 1)
		{
			LedOn(LED_RGB_B);
			LedOff(LED_RGB_G);
			SendCoefs(intention);
		}
		else if(systick_cont%4 == 2)
		{
			LedOn(LED_RGB_R);
			LedOff(LED_RGB_B);
			SendCoefs(repose);
		}
		else if(systick_cont%4 == 3)
		{
			LedOn(LED_RGB_B);
			LedOff(LED_RGB_R);
			SendCoefs(repose);
		}
	}
	else if (systick_cont >= 3)
		LedOn(LED_RGB_B);
}
/*==================[end of file]============================================*/

